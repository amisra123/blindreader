package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class BRCamera extends AppCompatActivity {
    private TextToSpeech t1;
    final Handler handler = new Handler();

    Conversion conv;

    String address = null , name=null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    Set<BluetoothDevice> pairedDevices;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private void bluetooth_connect_device() throws IOException
    {
        try
        {
            myBluetooth = BluetoothAdapter.getDefaultAdapter();
            address = myBluetooth.getAddress();
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size()>0)
            {
                for(BluetoothDevice bt : pairedDevices)
                {
                    address=bt.getAddress().toString();name = bt.getName().toString();
                    Toast.makeText(getApplicationContext(),"Connected", Toast.LENGTH_SHORT).show();

                }
            }

        }
        catch(Exception we){}
        myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
        btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
        btSocket.connect();
        Toast.makeText(this,name+"\n"+address,Toast.LENGTH_LONG).show();
    }

    private void sendArduino(char ch)
    {

        try
        {
            if (btSocket!=null) {
                Log.d("Arduinomethod", "Inside arduino method");
                String bin = conv.conversion(ch);
                btSocket.getOutputStream().write(Character.toString(ch).getBytes());

            }




        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_br);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);


        try{
            bluetooth_connect_device();
        }catch(Exception e){}
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @SuppressLint("NewApi")
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("You are inside the camera activity", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 1000);

        Button btnImgToTxt = findViewById(R.id.img_to_txt);
        Button btnObjectToText = findViewById(R.id.object_detection);
        Button btnExit = findViewById(R.id.exit_camera_br);

        btnObjectToText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArduino('o');
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(BRCamera.this, DetectorActivity.class));
                    }
                },400);
            }
        });
        btnImgToTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArduino('i');
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(BRCamera.this, BlindReader.class));
                    }
                },400);
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BRCamera.this, MainActivity.class));
            }
        });
    }
    @Override
    protected void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }
}
