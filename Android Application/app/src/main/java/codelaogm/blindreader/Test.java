package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class Test extends AppCompatActivity {
    private Button beginner;
    private Button intermediate;
    private Button custom;
    private Button tapAnswer;
    private Button stopAndExit;
    private TextToSpeech t1;
    final Handler handler = new Handler();
    int score,totalScore;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @SuppressLint("NewApi")
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("Test time! Good luck!", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        beginner=findViewById(R.id.button15);
        intermediate=findViewById(R.id.button16);
        custom=findViewById(R.id.button17);
        tapAnswer=findViewById(R.id.button19);
        stopAndExit=findViewById(R.id.button20);
        tapAnswer.setVisibility(View.GONE);
        stopAndExit.setVisibility(View.GONE);
        final Button exit = findViewById(R.id.btnExitTest);

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Test.this, Learn.class));
            }
        });

        final String alphabets[]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        final String words1[]={"Apple","Ball","Car","Doll","Ears","Face","God","Hat","Ice","Jug","King","Leg","Mouth","Nose","Ox",
                "Pants","Queen","Run","Stop","Tree","Umbrella","Van","Wind","Xylophone","You","Zero"};
        final String words2[]={"Airplane","Bat","Cap","Dress","English","Fall","Good","Hole","Inside","Joy","Keys","Lost","Money",
                "Noise","Orange","Pebble","Quiver","Rest","Sound","Turn","Up","Vaccine","Waist","X Mas","Yolk","Zip"};

        beginner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    int x=(int)(Math.random()*10000);
                    x=x%26;
                    String s=alphabets[x];
                    Log.d("TestTest",s);
                    //stream first s to device
                    tapAnswer.setVisibility(View.VISIBLE);
                    stopAndExit.setVisibility(View.VISIBLE);
                    beginner.setVisibility(View.GONE);
                    intermediate.setVisibility(View.GONE);
                    custom.setVisibility(View.GONE);
                    exit.setVisibility(View.GONE);
                    tapAnswer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //get string from voice
                            //check it it equals to s
                            //if so score++, totalScore++
                            //else totalScore++
                            int x=(int)(Math.random()*10000);
                            x=x%26;
                            String s=alphabets[x];
                            Log.d("TestTest",s);
                            //stream next s to device
                        }
                    });


            }
        });
        stopAndExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //speak score
                startActivity(new Intent(Test.this,Test.class));
            }
        });

        intermediate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x=(int)(Math.random()*10000);
                x=x%26;
                String s;
                int oddEven=(int)(Math.random()*10000);
                if(oddEven%2==0){s=words1[x];}else{s=words2[x];}
                //stream first s to device
                tapAnswer.setVisibility(View.VISIBLE);
                stopAndExit.setVisibility(View.VISIBLE);
                beginner.setVisibility(View.GONE);
                intermediate.setVisibility(View.GONE);
                custom.setVisibility(View.GONE);
                exit.setVisibility(View.GONE);
                tapAnswer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //get string from voice
                        //check it it equals to s
                        //if so score++, totalScore++
                        //else totalScore++
                        int x=(int)(Math.random()*10000);
                        x=x%26;
                        String s;
                        int oddEven=(int)(Math.random()*10000);
                        if(oddEven%2==0){s=words1[x];}else{s=words2[x];}
                        Log.d("TestTest",s);
                        //stream next s to device
                    }
                });
            }
        });
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Test.this,CustomStudy.class).putExtra("fromWhere","test"));
            }
        });


    }
    @Override
    protected void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }
}
