package codelaogm.blindreader;

public class Location {
    private double Latitude;
    private double Longitude;
    private String direction;

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public void setDirections(String val) {
        direction = val;
    }

    public double getLatitude() {
        return Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public String getDirections() {
        return direction;
    }

}
