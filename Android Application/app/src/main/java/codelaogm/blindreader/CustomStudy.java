package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class CustomStudy extends AppCompatActivity {
    Button submitandstream,tapToSpeak,stopAndExit;
    EditText et1,et2,et3,et4,et5,et6,et0;
    TextView tv;
    int score,totalScore;
    static int sIndex=1;
    String s1,s2,s3,s4,s5,s6,s7;
    private TextToSpeech t1;
    final Handler handler = new Handler();
    Conversion conv;

    String address = null , name=null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    Set<BluetoothDevice> pairedDevices;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private void bluetooth_connect_device() throws IOException
    {
        try
        {
            myBluetooth = BluetoothAdapter.getDefaultAdapter();
            address = myBluetooth.getAddress();
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size()>0)
            {
                for(BluetoothDevice bt : pairedDevices)
                {
                    address=bt.getAddress().toString();name = bt.getName().toString();
                    Toast.makeText(getApplicationContext(),"Connected", Toast.LENGTH_SHORT).show();

                }
            }

        }
        catch(Exception we){}
        myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
        btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
        btSocket.connect();
        Toast.makeText(this,name+"\n"+address,Toast.LENGTH_LONG).show();
    }
    private void sendArduino2(String i)
    {

        try
        {
            if (btSocket!=null)
            {
                for(int j=0;j<i.length();j++){

                    Log.d("Arduinomethod", "Inside arduino method") ;
                    String bin=conv.conversion(i.charAt(j));
                    bin+=",";
                    for(int k=0;k<bin.length();k++){
                        char z=bin.charAt(k);
                        btSocket.getOutputStream().write(Character.toString(z).getBytes());
                    }

                } }

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customstudy);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);


        try{
            bluetooth_connect_device();
        }catch(Exception e){}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @SuppressLint("NewApi")
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("Ready to study", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        final String fromWhere=getIntent().getStringExtra("fromWhere");
        submitandstream=findViewById(R.id.button18);
        et0=findViewById(R.id.textView11);
        et1=findViewById(R.id.textView12);
        et2=findViewById(R.id.textView13);
        et3=findViewById(R.id.textView14);
        et4=findViewById(R.id.textView15);
        et5=findViewById(R.id.textView16);
        et6=findViewById(R.id.textView17);
        tv=findViewById(R.id.textView21);
        tapToSpeak=findViewById(R.id.tapToSpeak);
        stopAndExit=findViewById(R.id.exitTapSpeak);
        tapToSpeak.setVisibility(View.GONE);
        stopAndExit.setVisibility(View.GONE);
        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                et1.setText("");
                et2.setText("");
                et3.setText("");
                et4.setText("");
                et5.setText("");
                et6.setText("");
                et0.setText("");
            }
        },3000);
        submitandstream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                s1=et0.getText().toString();
                s2=et1.getText().toString();
                s3=et2.getText().toString();
                s4=et3.getText().toString();
                s5=et4.getText().toString();
                s6=et5.getText().toString();
                s7=et6.getText().toString();
                et0.setVisibility(View.GONE);
                et1.setVisibility(View.GONE);
                et2.setVisibility(View.GONE);
                et3.setVisibility(View.GONE);
                et4.setVisibility(View.GONE);
                et5.setVisibility(View.GONE);
                Log.d("CustomTest",s5);
                String srr[]={s1,s2,s3,s4,s5,s6,s7};
                for(String s:srr){
                    if(s.equals("")){
                        tv.setText("Please fill all boxes");

                    }else{tv.setText("");}
                }
                if(fromWhere.equals("study")){
                    //stream s1...s8 at gaps of 4 seconds in voice and to device
                    sendArduino2(s1);
                    t1.speak(s1, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s2);
                    t1.speak(s2, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s3);
                    t1.speak(s3, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s4);
                    t1.speak(s4, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s5);
                    t1.speak(s5, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s6);
                    t1.speak(s6, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                    sendArduino2(s7);
                    t1.speak(s7, TextToSpeech.QUEUE_FLUSH, null);
                    try{Thread.sleep(4000);}catch (Exception e){}
                }else{
                    submitandstream.setVisibility(View.GONE);
                    tapToSpeak.setVisibility(View.VISIBLE);
                    stopAndExit.setVisibility(View.VISIBLE);
                    //stream srr[0] to device
                    sendArduino2(s1);

                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //get string from voice
                            //compare with srr[0]
                            //if correct score++, totalScore++;
                            //else totalScore++
                            //send srr[i]
                            getSpeechInput1();
                            sIndex++;

                        }
                    });
                    sendArduino2(s2);

                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput2();
                            sIndex++;
                        }
                    });
                    sendArduino2(s3);
                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput3();
                            sIndex++;
                        }
                    });
                    sendArduino2(s4);
                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput4();
                            sIndex++;
                        }
                    });
                    sendArduino2(s5);
                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput5();
                            sIndex++;
                        }
                    });
                    sendArduino2(s6);
                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput6();
                            sIndex++;
                        }
                    });
                    sendArduino2(s7);
                    tapToSpeak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getSpeechInput7();
                            sIndex++;
                        }
                    });


                    stopAndExit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d("SCORE", "onClick: stopAndExit: Line 260 Score:" + score);
                            startActivity(new Intent(CustomStudy.this,Learn.class));
                        }
                    });

                }

            }
        });

    }
    public void getSpeechInput1()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput2()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 11);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput3()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 12);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput4()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 13);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput5()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 14);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput6()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 15);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    public void getSpeechInput7()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 16);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case 10:
                if(requestCode==10 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s1)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                }break;
            case 11:
                if(requestCode==11 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s2)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            case 12:
                if(requestCode==12 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s3)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            case 13:
                if(requestCode == 13 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s4)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            case 14:
                if(requestCode == 14 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s5)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            case 15:
                if(requestCode == 15 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s6)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            case 16:
                if(requestCode == 16 && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains(s7)) {
                        score++;

                    }
                    else {
                        totalScore++;
                    }
                    break;
                }
            default:
                Toast.makeText(this, "Invalid choice", Toast.LENGTH_SHORT).show();
        }
        Log.d("SCORE", "onActivityResult: TotalScore:" + totalScore + "\nScore: " + score);
        t1.speak("your total score is"+totalScore, TextToSpeech.QUEUE_FLUSH, null);
    }
}
