package codelaogm.blindreader;

import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class SpeakActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speak);
        Button btnSpeak = (Button) findViewById(R.id.btnSpeak);
        Button btnExit = (Button) findViewById(R.id.btnExit);

        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSpeechInput();
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(new Intent(SpeakActivity.this, MainActivity.class)));
            }
        });
    }
    public void getSpeechInput()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if(resultCode == RESULT_OK && data != null)
                {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains("image to text")) {
                        Intent i = new Intent(SpeakActivity.this, BlindReader.class);
                        startActivity(i);
                    }
                    else if(result.get(0).contains("speech to text")) {
                        Intent i1 = new Intent(SpeakActivity.this, AudioToText.class);
                        startActivity(i1);
                    }
                    else if(result.get(0).contains("object detection")) {
                        Intent i2 = new Intent(SpeakActivity.this, DetectorActivity.class);
                        startActivity(i2);
                    }
                    else if(result.get(0).contains("settings")) {
                        Intent i3 = new Intent(SpeakActivity.this, Settings.class);
                        startActivity(i3);
                    }
                    else if(result.get(0).contains("tools")) {
                        Intent i4 = new Intent(SpeakActivity.this, BRTools.class);
                        startActivity(i4);
                    }
                    else if(result.get(0).contains("navigation")) {
                        Intent i5 = new Intent(SpeakActivity.this, Navig.class);
                        startActivity(i5);
                    }
                    else if(result.get(0).contains("calculator")) {
                        Intent i6 = new Intent(SpeakActivity.this, Calculator.class);
                        startActivity(i6);
                    }
                    else if(result.get(0).contains("learn")) {
                        Intent i7 = new Intent(SpeakActivity.this, Learn.class);
                        startActivity(i7);
                    }
                    else if(result.get(0).contains("exit")){
                        finish();
                    }
                    else {
                        Toast.makeText(this, "Invalid input", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
}
