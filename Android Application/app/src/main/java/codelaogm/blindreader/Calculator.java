package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.mariuszgromada.math.mxparser.Expression;

import java.util.ArrayList;
import java.util.Locale;

import codelaogm.blindreader.R;

public class Calculator extends AppCompatActivity {
    private Button btnAns, btnSub, btnAdd, btnMul, btnDiv, btnPercent, btnOpenbrack, btnClosebrack, btnOne, btnTwo, btnthree, btnFour, btnFive, btnSix, btnSeven, btnEight, btnNine, btnZero, btnCancel, btnEdit, btnSpeak;
    private TextView tvResult;
    private TextToSpeech t1;
    final Handler handler = new Handler();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cal);
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @SuppressLint("NewApi")
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("calculator opened", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);


        tvResult = (TextView)findViewById(R.id.tvResult);
        btnAns = (Button)findViewById(R.id.btnAnswer);
        btnSub = (Button)findViewById(R.id.btnSubtract);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnMul = (Button)findViewById(R.id.btnMul);
        btnDiv = (Button)findViewById(R.id.btnDivide);
        btnPercent = (Button)findViewById(R.id.btnPercentage);
        btnOpenbrack = (Button)findViewById(R.id.btnOpenbracket);
        btnClosebrack = (Button)findViewById(R.id.btnClosebrackrt);
        btnOne = (Button)findViewById(R.id.btn1);
        btnTwo = (Button)findViewById(R.id.btn2);
        btnthree = (Button)findViewById(R.id.btn3);
        btnFour= (Button)findViewById(R.id.btn4);
        btnFive = (Button)findViewById(R.id.btn5);
        btnSix = (Button)findViewById(R.id.btn6);
        btnSeven = (Button)findViewById(R.id.btn7);
        btnEight = (Button)findViewById(R.id.btn8);
        btnNine = (Button)findViewById(R.id.btn9);
        btnZero = (Button)findViewById(R.id.btn0);
        btnEdit = (Button)findViewById(R.id.btnEdit);
        btnCancel = (Button)findViewById(R.id.btnClear);
        btnSpeak = (Button)findViewById(R.id.btnSpeak);

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
        btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("1");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "one",Toast.LENGTH_SHORT).show();
                t1.speak("one", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("2");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "two",Toast.LENGTH_SHORT).show();
                t1.speak("two", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("3");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "three",Toast.LENGTH_SHORT).show();
                t1.speak("three", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("4");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "four",Toast.LENGTH_SHORT).show();
                t1.speak("four", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("5");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "five",Toast.LENGTH_SHORT).show();
                t1.speak("five", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("6");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "six",Toast.LENGTH_SHORT).show();
                t1.speak("six", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("7");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "seven",Toast.LENGTH_SHORT).show();
                t1.speak("seven", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("8");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "eight",Toast.LENGTH_SHORT).show();
                t1.speak("eight", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("9");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "nine",Toast.LENGTH_SHORT).show();
                t1.speak("nine", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("0");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "zero",Toast.LENGTH_SHORT).show();
                t1.speak("zero", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("-");
                Toast.makeText(getApplicationContext(), "minus",Toast.LENGTH_SHORT).show();
                t1.speak("minus", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("+");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "plus",Toast.LENGTH_SHORT).show();
                t1.speak("plus", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("*");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "into",Toast.LENGTH_SHORT).show();
                t1.speak("into", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("/");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "divided by",Toast.LENGTH_SHORT).show();
                t1.speak("divided by", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnPercent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("%");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "modulus",Toast.LENGTH_SHORT).show();
                t1.speak("modulus", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnOpenbrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append("(");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "open braces",Toast.LENGTH_SHORT).show();
                t1.speak("open braces", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnClosebrack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.append(")");
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "close braces",Toast.LENGTH_SHORT).show();
                t1.speak("close braces", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), toSpeak,Toast.LENGTH_SHORT).show();
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvResult.setText(null);
                //String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), "all cleared",Toast.LENGTH_SHORT).show();
                t1.speak("all cleared", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String reqstring = tvResult.getText().toString();
                reqstring = reqstring.substring(0, reqstring.length()-1);
                tvResult.setText(reqstring);
                Toast.makeText(getApplicationContext(), "last character removed",Toast.LENGTH_SHORT).show();
                t1.speak("deleted", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        btnAns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Expression e=new Expression(tvResult.getText().toString());
                final double ee=e.calculate();
                String str = String.valueOf(ee);
                tvResult.setText(str);
                String toSpeak = tvResult.getText().toString();
                Toast.makeText(getApplicationContext(), toSpeak,Toast.LENGTH_SHORT).show();
                t1.speak("The answer is" + toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            }
        });





    }
    public void onPause(){
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }

}
