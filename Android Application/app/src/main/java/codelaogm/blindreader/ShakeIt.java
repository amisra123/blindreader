package codelaogm.blindreader;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class ShakeIt extends Service implements SensorEventListener{
    SensorManager sm;
    double x,y,z,cac,lac,shake;
    static boolean isActive = false;
    public static final String CHANNEL_ID = "shakeit";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public ShakeIt() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isActive = true;
        Log.d("servshk","service is active");
        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        sm.registerListener(this,sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
        cac=SensorManager.GRAVITY_EARTH;
        lac=SensorManager.GRAVITY_EARTH;
        shake=0;

    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID).setContentTitle("Blind reader").setContentText("hello").build();
        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
       // Log.d("servshk","value changed");
        x=sensorEvent.values[0];
        y=sensorEvent.values[1];
        z=sensorEvent.values[2];
       // Log.d("servshk","x"+Double.toString(x));
        lac=cac;
        cac= Math.sqrt(x*x+y*y+z*z);
        double change=cac-lac;
        shake= shake + change;
       // Log.d("servshk",Double.toString(shake));
        if(shake>10){

            if(!MainActivity.isVisible) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
    }
}
