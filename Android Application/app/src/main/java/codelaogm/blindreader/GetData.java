package codelaogm.blindreader;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetData extends AsyncTask {
    private String test_output="";
    String spoken;
    StringBuilder spoken_format=new StringBuilder("");
    private LocationManager locationManager;
    public GetData(String spoken){
        this.spoken=spoken;
        for(int i=0;i<spoken.length();i++){
            char ch=spoken.charAt(i);
            if(ch==' '){
                spoken_format.append('+');
            }else{
                spoken_format.append(ch);
            }
        }

    }

    @Override
    protected Object doInBackground(Object[] objects) {
        try{
            URL urlDest = new URL("https://us1.locationiq.com/v1/search.php?key=a985fe61d1f952&q="+spoken_format+"&format=json");
            JSONArray destLatLong = GetJSONArray(urlDest);
            JSONObject jobs = destLatLong.getJSONObject(0);
            Location destinationLocation = new Location();
            destinationLocation.setLatitude(jobs.getDouble("lat"));
            destinationLocation.setLongitude(jobs.getDouble("lon"));
            Log.d("TAG_Loc", "Destination\nLat: " + destinationLocation.getLatitude() + "\tLong: " + destinationLocation.getLongitude());
            String ur="https://route.api.here.com/routing/7.2/calculateroute.json?app_id=mxw8hz8ou1SAk4woJ3TB&app_code=IAYBMTV9hHUsSsDX7VRglA&waypoint0=" +
                    "geo!22.346010,87.231972&waypoint1=geo!"+destinationLocation.getLatitude()+","+destinationLocation.getLongitude()+"&mode=fastest;car;traffic:disabled";
            URL url =new URL(ur);
            String data = "";
            JSONObject jo = GetJSONObject(url);//JSON Object to extract the directions
            JSONArray array=jo.getJSONObject("response").getJSONArray("route").getJSONObject(0).getJSONArray("leg").getJSONObject(0).getJSONArray("maneuver");
            Location[] locations = new Location[array.length()];
            for(int i=0;i<array.length();i++){
                locations[i] = new Location();
                locations[i].setLatitude(array.getJSONObject(i).getJSONObject("position").getDouble("latitude"));
                locations[i].setLongitude(array.getJSONObject(i).getJSONObject("position").getDouble("longitude"));
                String dirs=clipHtml(array.getJSONObject(i).getString("instruction"));
                locations[i].setDirections(dirs);

            }
            for (Location l : locations) {
                test_output+="\n"+l.getLatitude() + "\n"+l.getLongitude() + "\n"+l.getDirections();
                Log.d("NAV", "doInBackground: " + l.toString());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject GetJSONObject(URL url) {
        JSONObject jo = null;
        String data = "";
        try {
            HttpURLConnection huc=(HttpURLConnection)url.openConnection();
            InputStream is=huc.getInputStream();
            BufferedReader br=new BufferedReader(new InputStreamReader(is));
            String line="";
            while(line!=null){
                line=br.readLine();
                data=data+line; //whole json
            }
            jo = new JSONObject(data);
        } catch (Exception e) {
            Log.d("JSON Error", e.toString());
        }
        return jo;
    }

    private JSONArray GetJSONArray(URL url) {
        JSONArray jo = null;
        String data = "";
        try {
            HttpURLConnection huc=(HttpURLConnection)url.openConnection();
            InputStream is=huc.getInputStream();
            BufferedReader br=new BufferedReader(new InputStreamReader(is));
            String line="";
            while(line!=null){
                line=br.readLine();
                data=data+line; //whole json
            }
            jo = new JSONArray(data);
        } catch (Exception e) {
            Log.d("JSON Error", e.toString());
        }
        return jo;
    }


    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Navig.ft.setText(this.test_output);
        Log.d("Navigtt",test_output);
    }

    protected String clipHtml(String dirs){
        StringBuilder sb=new StringBuilder("");
        int flag=0,control=1;
        for(int i=0;i<dirs.length();i++){
            char ch=dirs.charAt(i);
            if(ch=='<'){
                flag=1;
                control=0;
            }
            else if(ch=='>'&&flag==1){
                flag=0;
                control=1;
            }
            else if(control==1){
                sb.append(ch);
            }

        }
        Log.d("dirs",sb.toString());
        return sb.toString();
    }
}