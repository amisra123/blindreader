package codelaogm.blindreader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.nfc.Tag;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.SyncStateContract;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {
    public static Context applicationContext;
    public static final String CHANNEL_ID = "shakeit";
    String path="";
    MediaRecorder mr;
    final int REQUEST_PERMISSION_CODE=1000;
    static int timer_value;
    static int knockCount;
    static int KNOCK_SENSITIVITY;
    private static final String TAG = "MainActivity";
    Vibrator v;
    public static boolean isVisible=false;
    private TextToSpeech t1;
    final Handler handler = new Handler();
    Conversion conv;

    String address = null , name=null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    Set<BluetoothDevice> pairedDevices;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private void bluetooth_connect_device() throws IOException
    {
        try
        {
            myBluetooth = BluetoothAdapter.getDefaultAdapter();
            address = myBluetooth.getAddress();
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size()>0)
            {
                for(BluetoothDevice bt : pairedDevices)
                {
                    address=bt.getAddress().toString();name = bt.getName().toString();
                    Toast.makeText(getApplicationContext(),"Connected", Toast.LENGTH_SHORT).show();

                }
            }

        }
        catch(Exception we){}
        myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
        btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
        btSocket.connect();
        Toast.makeText(this,name+"\n"+address,Toast.LENGTH_LONG).show();
    }

    private void sendArduino(char ch)
    {

        try
        {
            if (btSocket!=null) {
                Log.d("Arduinomethod", "Inside arduino method");
                String bin = conv.conversion(ch);
                btSocket.getOutputStream().write(Character.toString(ch).getBytes());

            }




        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }





    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        applicationContext=getApplicationContext();

        setVolumeControlStream(AudioManager.STREAM_MUSIC);


        try{
            bluetooth_connect_device();
        }catch(Exception e){}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @SuppressLint("NewApi")
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("You are in home page of blind reader app", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        knockCount=0;
        timer_value=0;



        v=(Vibrator)getSystemService(this.VIBRATOR_SERVICE);

        if(!checkPermissionFromDevice())
            requestPermission();
        else{
            Log.d("rec","granted");
        }

        SettingsConfig sc=new SettingsConfig();
        sc.getHistory();

        if(sc.getSpeech_Mode()==1){
            sc.setSpeech_Mode(0);
            sc.setHistory();
            Log.d(TAG, "onCreate: " + sc.getSpeech_Mode());
            startActivity(new Intent(MainActivity.this, SpeakActivity.class));
            //speech stuff on
        }else{
            //speech stuff off
        }

        if(sc.getKnock_Mode()==1){
            if (checkPermissionFromDevice()) {
                path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                        "CodeLagom" + "_audio_rec.3gp";
                setUpMediaRc();
                try {
                    mr.prepare();
                    mr.start();
                    Inf inf=new Inf();
                    Thread tinf=new Thread(inf);
                    tinf.start();
                } catch (Exception e) {
                }
            }else{
                requestPermission();
            }

            if(sc.getKnock_Sensitivity()==1){
                KNOCK_SENSITIVITY=32000;
                Log.d("TimerKnock","Outdoor Mode");
            }else{
                KNOCK_SENSITIVITY=15000;
                Log.d("TimerKnock","Indoor Mode");
            }
        }else{
            //knock stuff off
        }

        if(sc.getShake_Mode()==1){
            if (!ShakeIt.isActive) {
                startService(new Intent(MainActivity.this,ShakeIt.class));
            }
        }else{
            if (ShakeIt.isActive) {
                stopService(new Intent(MainActivity.this,ShakeIt.class));
            }
        }


        Button btnExit = findViewById(R.id.button4);
        Button btnSTT = findViewById(R.id.speechToText);
        Button cameraBtn = findViewById(R.id.imageToText);
        btnSTT.setOnClickListener(new View.OnClickListener() {
		           @Override
		            public void onClick(View v) {
                        sendArduino('v');
                               try {
                                     mr.stop();
                                    } catch (Exception e) {}
                       Intent i=new Intent(MainActivity.this,SpeakActivity.class);
                       startActivity(i);
                      }
		    });
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mr.stop();
                }catch(Exception e){}
                sendArduino('c');
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, BRCamera.class));
                    }
                },400);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    mr.stop();
                }catch(Exception e){
                    Log.e(TAG, "btnExit.setOnClickListener: Line - 261" + e.toString() );
                }
                Intent i=new Intent(MainActivity.this,Settings.class);
                startActivity(i);
            }
        });
        Button btnTools = findViewById(R.id.button2);
       btnTools.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               sendArduino('t');
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       startActivity(new Intent(MainActivity.this, BRTools.class));
                   }
               },400);
           }
       });
       createNotificationChannel();


    }

    private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(CHANNEL_ID, "shake it", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public static Context getContext(){
        return applicationContext;
    }



    public void imageToText(View v) {
        try{
            mr.stop();
        }catch(Exception e){}
        Intent Student1 = new Intent(MainActivity.this, BlindReader.class);
        startActivity(Student1);

    }

    public void speechToText(View v) {
        try{
            mr.stop();
        }catch(Exception e){}
        Intent Student2 = new Intent(MainActivity.this, AudioToText.class);
        startActivity(Student2);

    }


    public void getSpeechInput()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
 }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if(resultCode == RESULT_OK && data != null)
                {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if(result.get(0).contains("image to text")) {
                        Intent i = new Intent(MainActivity.this, BlindReader.class);
                        startActivity(i);
                    }
                    else if(result.get(0).contains("speech to text")) {
                        Intent i1 = new Intent(MainActivity.this, AudioToText.class);
                        startActivity(i1);
                    }
                    else if(result.get(0).contains("exit")){
                        finish();
                    }
                    else {
                        Toast.makeText(this, "Invalid input", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }
    private boolean checkPermissionFromDevice(){
        int write_ext_storage_result= ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio=ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        return write_ext_storage_result== PackageManager.PERMISSION_GRANTED &&
                record_audio==PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSION_CODE:{
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){}
            }
            break;
        }
    }

    private void setUpMediaRc(){
        mr=new MediaRecorder();
        mr.setAudioSource(MediaRecorder.AudioSource.MIC);
        mr.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mr.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mr.setOutputFile(path);

    }
    class Inf implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                if(timer_value == 1) {
                    Log.d("TimerKnock","Enters here");
                    timer_value=0;
                    if (knockCount==3) {
                        Log.d("TimerKnock","Knocked Thrice");
                        try{mr.stop();}catch(Exception e){
                            Log.e(TAG, "run: " + e.toString() );
                        }
                       Intent it = new Intent(MainActivity.this, Navig.class);
                       startActivity(it);
                    }
                    else if (knockCount == 2) {
                        Log.d("TimerKnock","Twice");
                        try{mr.stop();}catch(Exception e){
                            Log.e(TAG, "run: " + e.toString() );
                        }
                        Intent it = new Intent(MainActivity.this, BlindReader.class);
                        startActivity(it);

                    }else if(knockCount==1){
                        Log.d("TimerKnock","Once");
                        try{mr.stop();}catch(Exception e){
                            Log.e(TAG, "run: " + e.toString() );
                        }
                        Intent it=new Intent(MainActivity.this,Calculator.class);
                        startActivity(it);
                    }
                    Thread.currentThread().interrupt();
                    break;
                }

                if (mr != null) {
                    int d = mr.getMaxAmplitude();
                    if(d>KNOCK_SENSITIVITY) {
                        v.vibrate(500);

                        knockCount++;
                        Log.d("recpt", "Knocked");
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                if(knockCount==1) {
                                    new CountDownTimer(4000, 1000) {

                                        public void onTick(long millisUntilFinished) {
                                            Log.d("TimerKnock","timer started");
                                        }

                                        public void onFinish() {
                                            try {
                                                mr.stop();
                                            } catch (Exception e) {
                                                Log.e(TAG, "onFinish: " + e.toString() );
                                            }
                                            timer_value = 1;
                                            Log.d("TimerKnock",Integer.toString(timer_value));
                                        }

                                    }.start();
                                }
                            }
                        });
                    }
                }

            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        isVisible=true;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
        isVisible=false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible=false;
    }

}