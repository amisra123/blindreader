package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import java.util.Locale;

public class Settings extends AppCompatActivity {
    /******************************************************************************************************************************
     * Who TF in their right minds declares private variables with a Capital first letter? This is not how we code in Java.       *
     * Private variables should start with lower-case character.                                                                  *
     * Final global variables should be all caps.                                                                                 *
     * All methods should start with lower-case character.                                                                        *
     ******************************************************************************************************************************/
    private TextToSpeech t1;
    final Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @SuppressLint("NewApi")
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("You are in settings", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        final ToggleButton Speech=findViewById(R.id.button8);
        final ToggleButton Knock=findViewById(R.id.button9);
        final ToggleButton KnockS=findViewById(R.id.button10);
        final ToggleButton Shake=findViewById(R.id.button11);
        final Button Exit=findViewById(R.id.button12);
        final SettingsConfig sc=new SettingsConfig();
        sc.getHistory();
        if(sc.getSpeech_Mode()==0){
            Speech.setChecked(false);
            Speech.setText("OFF");
            Speech.setBackgroundColor(Color.parseColor("#b2bec3"));
            Speech.setTextColor(Color.parseColor("#000000"));
        } else {
            Speech.setChecked(true);
            Speech.setText("ON");
            Speech.setBackgroundColor(Color.parseColor("#6c5ce7"));
            Speech.setTextColor(Color.parseColor("#ffffff"));
        }
        if(sc.getKnock_Mode()==0){
            Knock.setChecked(false);
            Knock.setText("OFF");
            Knock.setBackgroundColor(Color.parseColor("#b2bec3"));
            Knock.setTextColor(Color.parseColor("#000000"));
        } else {
            Knock.setChecked(true);
            Knock.setText("ON");
            Knock.setBackgroundColor(Color.parseColor("#6c5ce7"));
            Knock.setTextColor(Color.parseColor("#ffffff"));}
        if(sc.getKnock_Sensitivity()==0){
            KnockS.setChecked(true);
            KnockS.setText("INDOOR");
            KnockS.setBackgroundColor(Color.parseColor("#00b894"));
        } else {
            KnockS.setChecked(false);
            KnockS.setText("OUTDOOR");
            KnockS.setBackgroundColor(Color.parseColor("#6c5ce7"));
        }
        if(sc.getShake_Mode()==0){
            Shake.setChecked(false);
            Shake.setText("OFF");
            Shake.setBackgroundColor(Color.parseColor("#b2bec3"));
            Shake.setTextColor(Color.parseColor("#000000"));
        } else {
            Shake.setChecked(true);
            Shake.setText("ON");
            Shake.setBackgroundColor(Color.parseColor("#6c5ce7"));
            Shake.setTextColor(Color.parseColor("#ffffff"));
        }

        Speech.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sc.setSpeech_Mode(1);
                    Speech.setTextOn("ON");
                    Speech.setBackgroundColor(Color.parseColor("#6c5ce7"));
                    Speech.setTextColor(Color.parseColor("#ffffff"));
                } else {
                    sc.setSpeech_Mode(0);
                    Speech.setTextOff("OFF");
                    Speech.setBackgroundColor(Color.parseColor("#b2bec3"));
                    Speech.setTextColor(Color.parseColor("#000000"));
                }
            }
        });

        Knock.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("ToggleButtons", "onCheckedChanged: " + isChecked);
                if (isChecked) {
                    sc.setKnock_Mode(1);
                    Knock.setTextOn("ON");
                    Knock.setBackgroundColor(Color.parseColor("#6c5ce7"));
                    Knock.setTextColor(Color.parseColor("#ffffff"));
                } else {
                    sc.setKnock_Mode(0);
                    Knock.setTextOff("OFF");
                    Knock.setBackgroundColor(Color.parseColor("#b2bec3"));
                    Knock.setTextColor(Color.parseColor("#000000"));
                }
            }
        });

        KnockS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    KnockS.setTextOn("INDOOR");
                    sc.setKnock_Sensitivity(0);
                    KnockS.setBackgroundColor(Color.parseColor("#00b894"));
                } else {
                    KnockS.setTextOff("OUTDOOR");
                    sc.setKnock_Sensitivity(1);
                    KnockS.setBackgroundColor(Color.parseColor("#6c5ce7"));

                }
            }
        });

        Shake.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sc.setShake_Mode(1);
                    Log.d("ShakeTest","here 1");
                    Shake.setTextOn("ON");
                    Shake.setBackgroundColor(Color.parseColor("#6c5ce7"));
                    Shake.setTextColor(Color.parseColor("#ffffff"));
                } else {
                    sc.setShake_Mode(0);
                    Log.d("ShakeTest","here 0");
                    Shake.setTextOff("OFF");
                    Shake.setBackgroundColor(Color.parseColor("#b2bec3"));
                    Shake.setTextColor(Color.parseColor("#000000"));
                }
            }
        });

        Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sc.setHistory();
                t1.speak("settings saved", TextToSpeech.QUEUE_FLUSH, null);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i=new Intent(Settings.this,MainActivity.class);
                        startActivity(i);
                    }
                },2000);
            }
        });



    }
    @Override
    protected void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }
}
