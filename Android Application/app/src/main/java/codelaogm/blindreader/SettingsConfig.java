package codelaogm.blindreader;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.midi.MidiDeviceService;
import android.preference.PreferenceManager;
import android.util.Log;

public class SettingsConfig {
    //0-OFF/Indoor //1-ON/Outdoor
    private int Speech_Mode=0;
    private int Knock_Mode=0;
    private int Knock_Sensitivity=0;
    private int Shake_Mode=0;
    private static final String TAG = "MainActivity";
    Context aC;

    public void setHistory(){
        aC= MainActivity.getContext();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(aC);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("Speech_Mode", Speech_Mode);
        editor.commit();
        editor.putInt("Knock_Mode", Knock_Mode);
        editor.commit();
        editor.putInt("Knock_Sensitivity", Knock_Sensitivity);
        editor.commit();
        editor.putInt("Shake_Mode", Shake_Mode);
        editor.commit();
    }

    public void getHistory(){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(MainActivity.getContext());
        Speech_Mode=preferences.getInt("Speech_Mode",0);
        Knock_Mode=preferences.getInt("Knock_Mode",0);
        Knock_Sensitivity=preferences.getInt("Knock_Sensitivity",0);
        Shake_Mode=preferences.getInt("Shake_Mode",0);
    }

    public void setSpeech_Mode(int mode){
        Log.d(TAG, "setSpeech_Mode: " + mode);
        Speech_Mode=mode;
    }
    public void setKnock_Mode(int mode){
        Knock_Mode=mode;
    }
    public void setKnock_Sensitivity(int mode){
        Knock_Sensitivity=mode;
    }
    public void setShake_Mode(int mode){
        Shake_Mode=mode;
    }
    public int getSpeech_Mode(){
        Log.d(TAG, "getSpeech_Mode: " + Speech_Mode);
        return Speech_Mode;
    }
    public int getKnock_Mode(){
        return Knock_Mode;
    }
    public int getKnock_Sensitivity(){
        return Knock_Sensitivity;
    }
    public int getShake_Mode(){
        return Shake_Mode;
    }
}
