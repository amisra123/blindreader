package codelaogm.blindreader;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.UUID;

import static java.lang.Thread.sleep;

public class Rec extends AppCompatActivity {
    Button start,stop;
    TextView tv;
    String path="";
    MediaRecorder mr;
    final int REQUEST_PERMISSION_CODE=1000;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_rec);
        start=findViewById(R.id.button5);
        stop=findViewById(R.id.button6);
        tv=findViewById(R.id.textView3);
        if(!checkPermissionFromDevice())
            requestPermission();
        else{
            Log.d("rec","granted");
        }


            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkPermissionFromDevice()) {
                        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                "CodeLagom" + "_audio_rec.3gp";
                        setUpMediaRc();
                        try {
                            mr.prepare();
                            mr.start();
                            Inf inf=new Inf();
                            Thread tinf=new Thread(inf);
                            tinf.start();
                        } catch (Exception e) {
                        }
                    }else{
                        requestPermission();
                    }
                }
            });
            stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mr.stop();
                }
            });



    }


    private void setUpMediaRc(){
          mr=new MediaRecorder();
          mr.setAudioSource(MediaRecorder.AudioSource.MIC);
          mr.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
          mr.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
          mr.setOutputFile(path);

    }

    private boolean checkPermissionFromDevice(){
        int write_ext_storage_result= ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio=ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        return write_ext_storage_result== PackageManager.PERMISSION_GRANTED &&
                record_audio==PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSION_CODE:{
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){}
            }
            break;
        }
    }
    class Inf implements Runnable {
        int i=1;
        @Override
        public void run() {
            while (true) {
                /**
                try {
                    sleep(250);

                } catch (Exception e) {
                    Log.d("recpt", "exception here in thread");
                }
                 **/
                if (mr != null) {
                    int d = mr.getMaxAmplitude();
                    if(d>30000) {
                        Log.d("recpt", "Knocked");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setText("Knocked" + " " + Integer.toString(i++));
                            }
                        });
                    }
                }

            }
        }
    }
}
