package codelaogm.blindreader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import static java.lang.Thread.sleep;

public class Navig extends AppCompatActivity implements LocationListener{

    private final int REQUEST_PERMISSION_CODE=1000;
    public static TextView ft;
    Location location;
    private TextToSpeech t1;
    final Handler handler = new Handler();
    String spoken;
    Button simulate_nav;
    Conversion conv;

    String address = null , name=null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    Set<BluetoothDevice> pairedDevices;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private void bluetooth_connect_device() throws IOException
    {
        try
        {
            myBluetooth = BluetoothAdapter.getDefaultAdapter();
            address = myBluetooth.getAddress();
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size()>0)
            {
                for(BluetoothDevice bt : pairedDevices)
                {
                    address=bt.getAddress().toString();name = bt.getName().toString();
                    Toast.makeText(getApplicationContext(),"Connected", Toast.LENGTH_SHORT).show();

                }
            }

        }
        catch(Exception we){}
        myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
        btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
        btSocket.connect();
        Toast.makeText(this,name+"\n"+address,Toast.LENGTH_LONG).show();
    }
    private void sendArduino(String i)
    {

        try
        {
            if (btSocket!=null)
            {
                for(int j=0;j<i.length();j++){

                    Log.d("Arduinomethod", "Inside arduino method") ;
                    String bin=conv.conversion(i.charAt(j));
                    bin+=",";
                    for(int k=0;k<bin.length();k++){
                        char z=bin.charAt(k);
                        btSocket.getOutputStream().write(Character.toString(z).getBytes());
                    }

                } }

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }

    public Navig() {
        super();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if(!checkForPermission()){
            requestPermission();
        }else{
            Log.d("Navigt","granted");
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sttservice);
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @SuppressLint("NewApi")
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("navigation opened", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        Button button = findViewById(R.id.button);
        Button speak = findViewById(R.id.button7);
        Button exit = findViewById(R.id.btnExitNav);
        simulate_nav = findViewById(R.id.btnSimulate);
        simulate_nav.setVisibility(View.GONE);
        ft=findViewById(R.id.ft);
        simulate_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send "rrrrrr"
                sendArduino("rrrrrr");
                //say "go right"
                t1.speak("go right", TextToSpeech.QUEUE_FLUSH, null);
                try {
//                    Thread.sleep(2000);
                    //send "llllll"
                    sendArduino("llllll");
                    //say "go left"
                    t1.speak("Go left", TextToSpeech.QUEUE_FLUSH, null);
//                    Thread.sleep(3000);
                    //send "ffffff"
                    sendArduino("ffffff");
                    //say "go forward"
                    t1.speak("Go forward", TextToSpeech.QUEUE_FLUSH, null);
//                    Thread.sleep(2000);
                }catch(Exception e){}
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Navig.this, BRTools.class));
            }
        });

        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSpeechInput();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetData gd=new GetData(spoken); //give destination parameter
                gd.execute();
            }
        });

        if(checkForPermission()){
            Log.d("Navigtt","entered check for permission");
            LocationManager locMan=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
            location=locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            onLocationChanged(location);




        }

    }

    private boolean checkForPermission(){
        int loc_perm= ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int locc_perm= ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

        return loc_perm== PackageManager.PERMISSION_GRANTED && locc_perm==PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
        },REQUEST_PERMISSION_CODE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSION_CODE:{
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){}
            }
            break;
        }
    }
    public void getSpeechInput()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if(resultCode == RESULT_OK && data != null)
                {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    spoken=result.get(0);
                }
                break;
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        String latitude,longitude;
        if(location!=null) {
            latitude = Double.toString(location.getLatitude());
            longitude = Double.toString(location.getLongitude());
        }else{
            latitude = "22.346010";
            longitude = "87.231972";
        }
        Log.d("Navigtt","Hola"+latitude+longitude);


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    @Override
    protected void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }


}

