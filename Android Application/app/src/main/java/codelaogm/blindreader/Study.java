package codelaogm.blindreader;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class Study extends AppCompatActivity {
    private TextToSpeech t1;
    final Handler handler = new Handler();

    Conversion conv;

    String address = null , name=null;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    Set<BluetoothDevice> pairedDevices;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final String WORDS_1[]={"Apple","Ball","Car","Doll","Ears","Face","God","Hat","Ice","Jug","King","Leg","Mouth","Nose","Ox",
            "Pants","Queen","Run","Stop","Tree","Umbrella","Van","Wind","Xylophone","You","Zero"};
    private final String WORDS_2[]={"Airplane","Bat","Cap","Dress","English","Fall","Good","Hole","Inside","Joy","Keys","Lost","Money",
            "Noise","Orange","Pebble","Quiver","Rest","Sound","Turn","Up","Vaccine","Waist","X Mas","Yolk","Zip"};
    private final String ALPHABETS[]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    private static int index = 0;
    private static int Study_mode;


    private void bluetooth_connect_device() throws IOException
    {
        try
        {
            myBluetooth = BluetoothAdapter.getDefaultAdapter();
            address = myBluetooth.getAddress();
            pairedDevices = myBluetooth.getBondedDevices();
            if (pairedDevices.size()>0)
            {
                for(BluetoothDevice bt : pairedDevices)
                {
                    address=bt.getAddress().toString();name = bt.getName().toString();
                    Toast.makeText(getApplicationContext(),"Connected", Toast.LENGTH_SHORT).show();

                }
            }

        }
        catch(Exception we){}
        myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
        BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
        btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
        btSocket.connect();
        Toast.makeText(this,name+"\n"+address,Toast.LENGTH_LONG).show();
    }
    private void sendArduino(char ch)
    {

        try
        {
            if (btSocket!=null) {
                Log.d("Arduinomethod", "Inside arduino method");
                String bin = conv.conversion(ch)+",";
                btSocket.getOutputStream().write(Character.toString(ch).getBytes());

            }




        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }
    private void sendArduino2(String i)
    {

        try
        {
            if (btSocket!=null)
            {
                for(int j=0;j<i.length();j++){

                    Log.d("Arduinomethod", "Inside arduino method") ;
                    String bin=conv.conversion(i.charAt(j));
                    bin+=",";
                    for(int k=0;k<bin.length();k++){
                        char z=bin.charAt(k);
                        btSocket.getOutputStream().write(Character.toString(z).getBytes());
                    }

                } }

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);


        try{
            bluetooth_connect_device();
        }catch(Exception e){}
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
                @SuppressLint("NewApi")
                @Override
                public void onInit(int status) {
                    if(status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.UK);
                    }
                }
            });
        }
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    t1.speak("Ready to study", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        }, 600);
        final Button beginner = findViewById(R.id.button15);
        final Button intermediate = findViewById(R.id.button16);
        final Button custom = findViewById(R.id.button17);
        final Button exit = findViewById(R.id.btnExitStudy);
        final Button next = findViewById(R.id.clkNxt);
        final Button exitNext = findViewById(R.id.btnExitHidden);

        exitNext.setVisibility(View.GONE);

        exitNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next.setVisibility(View.GONE);
                exitNext.setVisibility(View.GONE);
                beginner.setVisibility(View.VISIBLE);
                intermediate.setVisibility(View.VISIBLE);
                custom.setVisibility(View.VISIBLE);
                exit.setVisibility(View.VISIBLE);
            }
        });

        next.setVisibility(View.GONE);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Study_mode == 0) {
                    beginnerNext(index);
                } else {
                    intermediateNext(index);
                }
                if (index < 26) {
                    index++;
                } else {
                    startActivity(new Intent(Study.this, MainActivity.class));
                }
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t1.speak("you have now, completed your training", TextToSpeech.QUEUE_FLUSH, null);
                startActivity(new Intent(Study.this, Learn.class));
            }
        });



        beginner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArduino('b');

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        beginner.setVisibility(View.GONE);
                        intermediate.setVisibility(View.GONE);
                        custom.setVisibility(View.GONE);
                        exit.setVisibility(View.GONE);
                        next.setVisibility(View.VISIBLE);
                        exitNext.setVisibility(View.VISIBLE);
                        Study_mode = 0;
                    }
                },400);
        }
        });
        intermediate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArduino('i');
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       beginner.setVisibility(View.GONE);
                       intermediate.setVisibility(View.GONE);
                       custom.setVisibility(View.GONE);
                       exit.setVisibility(View.GONE);
                       next.setVisibility(View.VISIBLE);
                       exitNext.setVisibility(View.VISIBLE);
                       Study_mode = 1;
                   }
               },400);
            }
        });
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendArduino('c');
                startActivity(new Intent(Study.this,CustomStudy.class).putExtra("fromWhere","study"));
            }
        });


    }
    protected void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }


    private void beginnerNext(int i) {
        int oddEven=(int)Math.random()*10000;
        String s;
        if(oddEven%2==0){s=WORDS_1[i];}else{s=WORDS_2[i];}
        sendArduino(ALPHABETS[i].charAt(0));
        try{Thread.sleep(400);}catch (Exception e){}
        t1.speak(ALPHABETS[i], TextToSpeech.QUEUE_FLUSH, null);
        try{Thread.sleep(1000);}catch(Exception e){}
        s=ALPHABETS[i]+" for "+s;
        Log.d("StudyTest",s);
        t1.speak(s, TextToSpeech.QUEUE_FLUSH, null);
        try{Thread.sleep(2000);}catch(Exception e){}
        Toast.makeText(this, "Flush activated", Toast.LENGTH_SHORT).show();
    }

    private void intermediateNext(int i) {
        int x=(int)(Math.random()*100);
        x=x%26;
        int oddEven=(int)(Math.random()*10000);
        String s;
        if(oddEven%2==0){
            s=WORDS_1[x];
        }else{
            s=WORDS_2[x];
        }
        Log.d("StudyTest",s);
        //TODO: SASWAT stream s
        //TODO: SASWAT speak s
        sendArduino2(s);
        t1.speak(s, TextToSpeech.QUEUE_FLUSH, null);
        try{Thread.sleep(3000);}catch(Exception e){}
    }
}
