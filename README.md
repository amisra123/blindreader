The link to repository is https://bitbucket.org/amisra123/blindreader/src/master/
or in git bash command : git clone https://amisra123@bitbucket.org/amisra123/blindreader.git


Steps to run the web app:-
1. Install pycharm 
2. Set the interpreter to Python 3.7
3. Import the dependencies mentioned in the requirement.txt file
4. Run the run.py file
5. Click on the link of the local host mentioned in the command line of pycharm
6. You will be routed to the Home page of the website where you can sign up and upload pdf.


Steps to run the android application:-
1. Copy the apk present in blindreader folder and install it in a android phone with min version android Oreo.
2. Clone the repository from https://bitbucket.org/amisra123/blindreader/src/master/
or in git bash command : git clone https://amisra123@bitbucket.org/amisra123/blindreader.git
3. Open the android application folder using android studio 3.1
4. Wait till the android studio syncs the files and installs everything.
5. Run the application using usb debugging or using a virtual device.
